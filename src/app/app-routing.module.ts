import { VerifyEmailComponent } from './component/verify-email/verify-email.component';
import { ForgetPassComponent } from './component/forget-pass/forget-pass.component';
import { DeshboardComponent } from './component/deshboard/deshboard.component';
import { RegisterComponent } from './component/register/register.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/login/login.component';

const routes: Routes = [
  {
    path:'', redirectTo:'login',pathMatch:'full'
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path:'register', component:RegisterComponent
  },
  {
    path:'deshboard',component:DeshboardComponent
  },
  {
    path:'forgetpass',component:ForgetPassComponent
  },
  {
    path:'verifyemail',component:VerifyEmailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
