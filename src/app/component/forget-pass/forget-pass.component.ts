import { Component } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-forget-pass',
  templateUrl: './forget-pass.component.html',
  styleUrls: ['./forget-pass.component.css']
})
export class ForgetPassComponent {

  constructor(private auth:AuthService){}

  email:string='';

  forgetpass(){
    this.auth.forgetpassward(this.email);
    this.email='';
  }

}
