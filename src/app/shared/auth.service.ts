import { Injectable } from '@angular/core';
import  { AngularFireAuth } from '@angular/fire/compat/auth'
import { Router } from '@angular/router';
import { GoogleAuthProvider, GithubAuthProvider, FacebookAuthProvider} from '@angular/fire/auth'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private fireauth:AngularFireAuth, private router:Router) { }

  //login

  login(email:string, password:string) {
    this.fireauth.signInWithEmailAndPassword(email,password).then ((res)=>{
      localStorage.setItem('token','true')


      if(res.user?.emailVerified == true){
        this.router.navigate(['/deshboard'])
      }
      else{
        this.router.navigate(['/verifyemail'])
      }
    },err=>{
        alert('Somethig went wrong')
        this.router.navigate(['/login'])
    })
  }

   //register

   register(email:string, password:string) {
    this.fireauth.createUserWithEmailAndPassword(email,password).then ((res)=>{
      alert('register sucessful')
      this.router.navigate(['/login'])
      // this.sendemailforverification(res.user)
      res.user?.sendEmailVerification().then(res=>{
        this.router.navigate(['/verifyemail'])
      });
    },err=>{
        alert(err.message)
        this.router.navigate(['/register'])
    })
  }

  //signout
  logout(){
    this.fireauth.signOut().then(()=>{
      localStorage.removeItem('token')
    this.router.navigate(['/login'])
  },err=>{
    alert(err.message);
  })
  }


  // forgetpass
  forgetpassward(email:string){
    this.fireauth.sendPasswordResetEmail(email).then(()=> {
    this.router.navigate(['/verifyemail'])
  },
  err=>{
    alert('something went wrong')
  })
}

// sendemailverification
// sendemailforverification(user:any){
// user.sendemailverification().then((res:any)=>{
//     this.router.navigate(['/verifyemail'])
// },
// (err:any)=>{
//   alert('something went wrong,not able to send email')
// })


// }


  //sign in with google
  googleSignIn() {
    return this.fireauth.signInWithPopup(new GoogleAuthProvider).then(res => {

      this.router.navigate(['/dashboard']);
      localStorage.setItem('token',JSON.stringify(res.user?.uid));

    }, err => {
      alert(err.message);
    })
  }
}
